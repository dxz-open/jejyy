function getOwerApplyList(community_id, replied,subscribed,success, page_size, page_num) {
    var sql = `
        select
            a.id,
            a.house,
            a.carport,
            a.warehouse,
            a.subscribed,
            a.replied,
            a.success,
            a.created_at,
            b.real_name
        from ejyy_ower_apply a 
        left join ejyy_wechat_mp_user b on b.id = a.wechat_mp_user_id        
        where 1=1
        ${replied?'and replied=:replied':''}
        ${subscribed?'and subscribed=:subscribed':''}
        ${success? 'and success=:success':''}
        and a.community_id=:community_id
        order by a.id desc
        limit ${(page_num-1)*page_size},${page_size}
    `;
    return sql;
}
function getOwerApplyListCount(community_id, replied,subscribed,success) {
    var where = '';
    if (replied != undefined) {
        where = 'a.replied=:replied and ';
    }
    if (subscribed != undefined) {
        where = where + 'a.subscribed=:subscribed and ';
    }
    if (success != undefined) {
        where = where + 'a.success=:success and ';
    }
    var sql = `
        select 
        count(*)
        from ejyy_ower_apply a 
        left join ejyy_wechat_mp_user b on b.id = a.wechat_mp_user_id
        where 1=1
        ${replied?'and replied=:replied':''}
        ${subscribed?'and subscribed=:subscribed':''}
        ${success? 'and success=:success':''}
        and a.community_id=:community_id
    `;
    return sql;
}

function getOwerApplyDetail(community_id,id) {
    var sql = `
        SELECT a.id,
               a.wechat_mp_user_id,
               a.community_name,
               a.house,
               a.carport,
               a.warehouse,
               a.subscribed,
               a.replied,
               a.replied_by,
               a.reply_content,
               a.replied_at,
               a.content,
               a.success,
               a.created_at,
               b.real_name,
               c.real_name AS replied_real_name
        FROM ejyy_ower_apply a
                 LEFT JOIN ejyy_wechat_mp_user b ON b.id = a.wechat_mp_user_id
                 LEFT JOIN ejyy_property_company_user c ON c.id = a.replied_by
        WHERE a.id = :id AND a.community_id =:community_id
        LIMIT 0,1
    `;
    return sql;
}

function getOwerApplyInfo(community_id,id) {
    var sql = `
        select * from ejyy_ower_apply a 
        where a.id=:id and a.community_id=:community_id
        limit 0,1
    `;
    return sql;
}