package cn.chowa.ejyy.common.utils;

/**
 * @ClassName ToolUtil
 * @Description TODO
 * @Author ironman
 * @Date 16:35 2022/8/25
 */
public class ToolUtil {

    public static String omit(String str,int len) {
        if (str == null) {
            return "";
        }
        if (str.length() < len) {
            return str;
        }
        return str.substring(0,len - 2) + "...";
    }
}
