package cn.chowa.ejyy.epidemic;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.PagedData;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.EpidemicQuery;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController("epidemicList")
@RequestMapping("/pc/epidemic")
public class list {

    @Autowired
    private EpidemicQuery epidemicQuery;

    @SaCheckRole(Constants.RoleName.YQFK)
    @VerifyCommunity(true)
    @PostMapping("/list")
    public PagedData<ObjData> getList(@RequestBody RequestData data) {
        int tourCode = data.getInt("tour_code", false, "^1|2|3$");
        int returnHometown = data.getInt("return_hometown", false, "^1|0$");
        long communityId = data.getCommunityId();

        return PagedData.of(data.getPageNum(), data.getPageSize(),
                epidemicQuery.getBuildingEpidemics(tourCode, returnHometown, communityId,
                        data.getPageSize(), data.getPageNum())
                , epidemicQuery.getBuildingEpidemicsCount(tourCode, returnHometown, communityId));
    }

}
