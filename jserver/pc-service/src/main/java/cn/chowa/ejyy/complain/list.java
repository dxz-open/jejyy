package cn.chowa.ejyy.complain;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.PagedData;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.ComplainQuery;
import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("complainList")
@RequestMapping("/pc/complain")
public class list {

    @Autowired
    private ComplainQuery complainQuery;

    @SaCheckRole(Constants.RoleName.TSJY)
    @VerifyCommunity(true)
    @PostMapping("/list")
    public PagedData<ObjData> getList(@RequestBody RequestData data) {
        Integer type = data.getInt("type", false, "^1|2$");
        Integer category = data.getInt("category", false, "^1|2|3|4|5|6|7$");
        Integer step = data.getInt("step", false, "^1|2|3|4$");
        String refer = data.getStr("refer", false, "^ower|colleague$");
        return PagedData.of(data,
                complainQuery.getComplains(type, category,
                        data.getCommunityId(), refer, step,
                        data.getPageSize(), data.getPageNum()
                ),
                complainQuery.getComplainsCount(type, category,
                        data.getCommunityId(), refer, step
                )
        );
    }

}
