package cn.chowa.ejyy.ower;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.WechatMpUserQuery;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static cn.chowa.ejyy.common.Constants.code.QUERY_ILLEFAL;

/**
 * @ClassName detail
 * @Description TODO
 * @Author ironman
 * @Date 15:52 2022/8/23
 */

@Slf4j
@RestController("owerDetail")
@RequestMapping("/pc/ower")
public class detail {

    @Autowired
    private WechatMpUserQuery wechatMpUserQuery;

    /**
     * 查询业主档案详情
     *
     */
    @SaCheckRole(Constants.RoleName.YZDA)
    @VerifyCommunity(true)
    @PostMapping("/detail")
    public Map<String,Object> detail(@RequestBody RequestData data) {
        int id = data.getInt("id", true, "/^\\d+$/");
        long communityId = data.getCommunityId();

        //查询 用户信息
        ObjData user = wechatMpUserQuery.getWechatMapUserInfoById(id);
        if (user == null) {
            throw new CodeException(QUERY_ILLEFAL, "非法获取用户信息");
        }
        return Map.of("userDetail",user);
    }
}
