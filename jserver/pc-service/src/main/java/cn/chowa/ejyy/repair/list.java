package cn.chowa.ejyy.repair;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.PagedData;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.RepairQuery;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController("repairList")
@RequestMapping("/pc/repair")
public class list {

    @Autowired
    private RepairQuery repairQuery;

    @SaCheckRole(Constants.RoleName.WXWF)
    @VerifyCommunity(true)
    @PostMapping("/list")
    public PagedData<ObjData> getList(@RequestBody RequestData data) {
        int repair_type = data.getInt("repair_type", false, "^1|2|3|4$");
        int step = data.getInt("step", false, "^1|2|3|4$");
        String refer = data.getStr("refer", false, "^ower|colleague$");

        return PagedData.of(data,
                repairQuery.getBuildingRepair(repair_type,
                        data.getCommunityId(), step, refer,
                        data.getPageSize(), data.getPageNum()),
                repairQuery.getBuildingRepairCount(repair_type,
                        data.getCommunityId(), step, refer)
        );
    }

}
